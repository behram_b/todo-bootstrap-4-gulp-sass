var express = require('express');
var router = express.Router();
var ctrlList = require('../controller/listController');

router.get('/', ctrlList.list);
router.post('/', ctrlList.insert);

module.exports = router;