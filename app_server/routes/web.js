var routeLogin = require('./loginRoutes');
var routeIndex = require('./indexRoutes');
var routeList = require('./listRoutes');
var routeLogout = require('./logoutRoutes');



module.exports = function(app) {
    app.use('/list', routeList);
    app.use('/logout', routeLogout);    
    app.use('/login', routeLogin);
    app.use('/', routeIndex);  

}