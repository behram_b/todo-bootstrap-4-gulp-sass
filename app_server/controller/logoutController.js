module.exports.logout = function(req, res) {
    localStorage.removeItem('auth');
    res.redirect('login');    
}